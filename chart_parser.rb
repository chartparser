#!/usr/bin/env ruby

# Copyright (c) 2008 Tom Adams <tom@holizz.com>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

require 'set'
require 'logger'

class Terminal
  attr_accessor :token
  alias_method :to_s, :token
  alias_method :to_tree, :token
  def initialize(token)
    @token=token
  end
  def eql?(other)
    other.is_a?(Terminal) and other.token==@token
  end
  alias_method :==, :eql?
  alias_method :===, :eql?
end

class Edge
  attr_accessor :i,:j,:category,:contents,:sub
  def initialize(i,j,category,contents,sub=[])
    @i=i
    @j=j
    @category=category
    @contents=contents
    @sub=sub
  end
  def eql?(other)
    other.is_a? Edge and hash == other.hash
  end
  alias_method :==, :eql?
  alias_method :===, :eql?
  def hash
    [@i,@j,@category,@contents,@sub].hash
  end
  def to_s
    "#{active? ? 'A' : 'I'}"+
    "<#{@i},#{@j},#{@category},[%s],[%s]>" % [@contents.join(', '),
                                              @sub.join(', ')]
  end
  def active?
    unfound and unfound.length != 0
  end
  def unfound
    @contents[@sub.length..-1]
  end
  def nextWord
    unfound[0]
  end
  def to_tree
    #raise Exception if active?
    unless @sub.empty?
      [@category, @sub.map{|e|e.to_tree}]
    else
      [@category, @contents.map{|e|e.to_tree}]
    end
  end
end

class Parser
  def initialize(grammar,log=Logger.new(STDERR))
    @grammar = grammar
    @log = log
  end

  def parse(text)
    active = Set.new
    inactive = Set.new
    agenda = []

    # Initialise agenda with known words
    text.each_with_index do |word,n|
      cat = nil
      @grammar.each do |cat,rule|
        for r in rule
          if r == [Terminal.new(word)]
            agenda << Edge.new(n,n+1,cat,[],[Terminal.new(word)])
          end
        end
      end
    end

    until agenda.empty?
      # Move one from agenda to chart
      edge = agenda.delete_at(0)
      if edge.active?
        active << edge
      else
        inactive << edge
      end
      # Add to the agenda from edges in the chart
      new_edges(active,inactive) do |edge|
        unless agenda.include?(edge) or (active+inactive).include?(edge)
          @log.debug("new_edges adding: #{edge}")
          agenda << edge 
        end
      end

      agenda.each do |edge|
        @log.debug("agenda: #{edge}")
      end
      (active+inactive).each do |edge|
        @log.debug("chart: #{edge}")
      end
      #gets
    end

    # Return all spanning parses as a Set
    inactive.select do |edge|
      edge.i == 0 and edge.j == text.length
    end.map do |edge|
      edge.to_tree
    end.to_set
  end

  def new_edges(active,inactive)
    bottom_up(active,inactive) do |edge|
      yield edge
    end
    fundamental(active,inactive) do |edge|
      yield edge
    end
  end

  def bottom_up(active,inactive)
    inactive.each do |edge|
      @grammar.each do |r,w|
        w.each do |ww|
          if ww[0] == edge.category
            newedge = Edge.new(edge.i,edge.i,r,ww)
            @log.debug("bottom_up: via #{edge} found #{newedge}")
            yield newedge
          end
        end
      end
    end
  end
  
  def fundamental(active,inactive)
    for a in active
      for i in inactive
        if a.j == i.i and a.nextWord == i.category
          newedge = Edge.new(a.i,i.j,a.category,a.contents,a.sub+[i])
          @log.debug("fundamental: via #{a} and #{i} found #{newedge}")
          yield newedge
        end
      end
    end
  end
end

if __FILE__ == $0

  log = Logger.new(STDERR)
  log.level = Logger::DEBUG
  log.formatter = lambda {|sev,time,pname,msg| "#{sev}: #{msg}\n"}

  shouldBe = Set.new([["S", [["NP", [["NP", ["time"]], ["NP", ["flies"]]]],
  ["VP", [["V", ["like"]], ["NP", [["Det", ["an"]], ["NP", ["arrow"]]]]]]]],
  ["S", [["NP", ["time"]], ["VP", [["V", ["flies"]], ["PP", [["PR", ["like"]],
  ["NP", [["Det", ["an"]], ["NP", ["arrow"]]]]]]]]]]])
  # time flies like an arrow
  # -> [time] [flies] [like an arrow]
  # -> [time flies] [like] [an arrow]

  grammar = {'S'=>[%w{NP VP}],
             'VP'=>[%w{V NP},
                    %w{V PP}],
             'NP'=>[[Terminal.new('time')],
                    [Terminal.new('flies')],
                    [Terminal.new('arrow')],
                    %w{Det NP},
                    %w{NP NP}],
             'PP'=>[%w{PR NP}],
             'PR'=>[[Terminal.new('like')]],
             'Det'=>[[Terminal.new('an')]],
             'V'=>[[Terminal.new('like')],
                   [Terminal.new('flies')]]}

  p = Parser.new(grammar,log)
  q = p.parse(%w{time flies like an arrow})

  if q == shouldBe
    puts "Test succeeded!"
    puts "Output was #{q.inspect}"
  else
    puts "Test failed!"
    puts "Should have been #{shouldBe.inspect}"
    puts "But was actually #{q.inspect}"
  end
end
